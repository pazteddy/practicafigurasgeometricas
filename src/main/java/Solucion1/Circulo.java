package Solucion1;

public class Circulo extends Figura{
    private double radio;

    public Circulo(double radio) {
        super("Circulo");
        this.radio = radio;
    }

    @Override
    public double getArea() {
        double area = Math.PI*Math.pow(radio,2);
        return Math.round(area * 100.0) / 100.0;
    }

    @Override
    public void dibujar() {
        System.out.println("  ** ");
        System.out.println("*     *");
        System.out.println("*     *");
        System.out.println("  **  ");
    }
}
