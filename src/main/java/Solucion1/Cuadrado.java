package Solucion1;

public class Cuadrado extends Figura{
    private double lado;

    public Cuadrado(double lado) {
        super("Cuadrado");
        this.lado = lado;
    }

    @Override
    public double getArea() {
        return Math.pow(this.lado, 2);
    }

    @Override
    public void dibujar() {
        for (int i = 0; i < this.lado; i++) {
            for (int j = 0; j < this.lado; j++) {
                System.out.print("*");
            }
            System.out.println("");
        }
    }
}
