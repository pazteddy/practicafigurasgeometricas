import Solucion1.*;


public class Main {
    public static void main(String[] args) {
        Rectangulo rectangulo = new Rectangulo(8,4);
        double area =rectangulo.getArea();
        rectangulo.dibujar();
        System.out.println(area);
        Cuadrado cuadrado = new Cuadrado(3);
        cuadrado.dibujar();
        System.out.println(cuadrado.getArea());
        TrianguloRectangulo trianguloRectangulo = new TrianguloRectangulo(8,2);
        trianguloRectangulo.dibujar();
        System.out.println(trianguloRectangulo.getArea());
        Circulo circulo = new Circulo(2);
        circulo.dibujar();
        System.out.println(circulo.getArea());
        Rombo rombo = new Rombo(16,12);
        rombo.dibujar();
        System.out.println(rombo.getArea());
    }
}
