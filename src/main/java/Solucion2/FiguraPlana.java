package Solucion2;

public abstract class FiguraPlana extends Figura{

    public FiguraPlana(String tipo) {
        super(tipo);
    }

    abstract double getArea(double base, double altura);
    abstract void dibujar();
}
