package Solucion2;

public class Rombo extends Figura {
    private double diagonalMayor;
    private double diagonalMenor;

    public Rombo(double diagonalMayor,double diagonalMenor) {
        super("Rombo");
        this.diagonalMayor = diagonalMayor;
        this.diagonalMenor = diagonalMenor;
    }

    @Override
    public double getArea() {
        return diagonalMayor*diagonalMenor*0.5;
    }

    @Override
    public void dibujar() {
        System.out.println(" * ");
        System.out.println("*  *");
        System.out.println(" * ");
    }
}
