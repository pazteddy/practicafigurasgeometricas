package Solucion2;

public class Cuadrado extends FiguraPlana {
    private double lado;

    public Cuadrado(double lado) {
        super("Cuadrado");
        this.lado = lado;
    }

    @Override
    double getArea(double base, double altura) {
        return Math.pow(base, 2);
    }

    @Override
    public void dibujar() {
        for (int i = 0; i < this.lado; i++) {
            for (int j = 0; j < this.lado; j++) {
                System.out.print("*");
            }
            System.out.println("");
        }
    }
}
